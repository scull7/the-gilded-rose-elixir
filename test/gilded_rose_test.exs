defmodule GildedRoseTest do
  use ExUnit.Case
  doctest GildedRose

  test "interface specification" do
    gilded_rose = GildedRose.new()

    [ vest, brie, mongoose, sulfuras, backstage, cake ] = GildedRose.items(gilded_rose)

    assert items_equal?(vest, GildedRose.Item.new("+5 Dexterity Vest", 10, 20))
    assert items_equal?(brie, GildedRose.Item.new("Aged Brie", 2, 0))
    assert items_equal?(mongoose, GildedRose.Item.new("Elixir of the Mongoose", 5, 7))
    assert items_equal?(sulfuras, GildedRose.Item.new("Sulfuras, Hand of Ragnaros", 0, 80))
    assert items_equal?(backstage, GildedRose.Item.new("Backstage passes to a TAFKAL80ETC concert", 15, 20))
    assert items_equal?(cake, GildedRose.Item.new("Conjured Mana Cake", 3, 6))

    assert :ok = GildedRose.update_quality(gilded_rose)

    [ vest, brie, mongoose, sulfuras, backstage, cake ] = GildedRose.items(gilded_rose)

    assert items_equal?(vest, GildedRose.Item.new("+5 Dexterity Vest", 9, 19))
    assert items_equal?(brie, GildedRose.Item.new("Aged Brie", 1, 1))
    assert items_equal?(mongoose, GildedRose.Item.new("Elixir of the Mongoose", 4, 6))
    assert items_equal?(sulfuras, GildedRose.Item.new("Sulfuras, Hand of Ragnaros", 0, 80))
    assert items_equal?(backstage, GildedRose.Item.new("Backstage passes to a TAFKAL80ETC concert", 14, 21))
    assert items_equal?(cake, GildedRose.Item.new("Conjured Mana Cake", 2, 4))
  end

  defp items_equal?(left, right) do
    Enum.all?([:name, :quality, :sell_in], fn key -> Map.get(left, key) == Map.get(right, key) end)
  end
end
