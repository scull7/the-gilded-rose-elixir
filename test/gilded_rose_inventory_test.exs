defmodule GildedRoseInventoryTest do
  alias GildedRose.Item.Appraised, as: Appraised
  use ExUnit.Case

  test "apply_conjured_quality_decay :: should add +1 decay to the known Conjured Mana Cake case" do
    item = Appraised.new(GildedRose.Item.new("Conjured Mana Cake", 3, 6))
    actual = GildedRose.Inventory.apply_conjured_quality_decay(item)
    # NOTE: the decay function should not affect the _sell_in_ value.
    expected = GildedRose.Item.new("Conjured Mana Cake", 3, 5)

    assert items_equal?(actual.item, expected)
  end

  test "regression :: property test found incongruency { :sell_in -2 :quality: 0 :name: Backstage passes to a TAFKAL80ETC concert" do
    name = "Backstage passes to a TAFKAL80ETC concert"
    item = Appraised.new(GildedRose.Item.new(name, -2, 0))

    v1 =
      Appraised.unwrap_item(GildedRoseInventoryPropertyTest.v1_apply_standard_quality_decay(item))

    v2 = Appraised.unwrap_item(GildedRose.Inventory.apply_standard_quality_decay(item))

    expected = {v1.name, v1.quality, v1.sell_in}
    actual = {v2.name, v2.quality, v2.sell_in}

    assert expected == actual
  end

  # Not a fan of the copy paste, but with only two cases I didn't want to
  # add another abstraction yet.
  defp items_equal?(left, right) do
    props = [:name, :quality, :sell_in]

    Enum.all?(props, fn key -> Map.get(left, key) == Map.get(right, key) end)
  end
end

defmodule GildedRoseInventoryPropertyTest do
  alias GildedRose.Item.Appraised, as: Appraised
  use ExUnit.Case
  use ExUnitProperties

  # Let's make sure that our two standard decay functions return the same thing.
  #
  # _sell_in_ - I'm using +/- 20 as it is maximal of any values present
  # _quality - I'm using 0..50 as it is the specified value range
  #
  # I'm also only using names of known cases.
  property "apply_standard_quality_decay and apply_standard_quality_decay2 return the same result" do
    check all(
            a <- StreamData.integer(-20..20),
            b <- StreamData.integer(0..50),
            c <-
              StreamData.member_of([
                "+5 Dexterity Vest",
                "Aged Brie",
                "Elixir of the Mongoose",
                "Sulfuras, Hand of Ragnaros",
                "Backstage passes to a TAFKAL80ETC concert",
                "Conjured Mana Cake"
              ])
          ) do
      item = Appraised.new(GildedRose.Item.new(c, a, b))
      v1 = Appraised.unwrap_item(v1_apply_standard_quality_decay(item))
      v2 = Appraised.unwrap_item(GildedRose.Inventory.apply_standard_quality_decay(item))

      expected = {v1.name, v1.quality, v1.sell_in}
      actual = {v2.name, v2.quality, v2.sell_in}

      assert expected == actual
    end
  end

  # The original implementation.
  def v1_apply_standard_quality_decay(%Appraised{item: item}) do
    item =
      cond do
        item.name != "Aged Brie" && item.name != "Backstage passes to a TAFKAL80ETC concert" ->
          if item.quality > 0 do
            if item.name != "Sulfuras, Hand of Ragnaros" do
              %{item | quality: item.quality - 1}
            else
              item
            end
          else
            item
          end

        true ->
          cond do
            item.quality < 50 ->
              item = %{item | quality: item.quality + 1}

              cond do
                item.name == "Backstage passes to a TAFKAL80ETC concert" ->
                  item =
                    cond do
                      item.sell_in < 11 ->
                        cond do
                          item.quality < 50 ->
                            %{item | quality: item.quality + 1}

                          true ->
                            item
                        end

                      true ->
                        item
                    end

                  cond do
                    item.sell_in < 6 ->
                      cond do
                        item.quality < 50 ->
                          %{item | quality: item.quality + 1}

                        true ->
                          item
                      end

                    true ->
                      item
                  end

                true ->
                  item
              end

            true ->
              item
          end
      end

    Appraised.new(item)
  end
end
