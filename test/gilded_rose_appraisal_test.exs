defmodule GildedRoseAppraisalTest do
  use ExUnit.Case

  # Make sure that our tokenizer works on all known cases

  test "tokenize :: known case - Conjured Mana Cake" do
    actual = GildedRose.Appraisal.tokenize("Conjured Mana Cake")
    expected = ["Conjured", "Mana", "Cake"]

    assert actual == expected
  end

  test "tokenize :: known case - +5 Dexterity Vest" do
    actual = GildedRose.Appraisal.tokenize("+5 Dexterity Vest")
    expected = ["+5", "Dexterity", "Vest"]

    assert actual == expected
  end

  test "tokenize :: known case - Aged Brie" do
    actual = GildedRose.Appraisal.tokenize("Aged Brie")
    expected = ["Aged", "Brie"]

    assert actual == expected
  end

  test "tokenize :: known case - Elixir of the Mongoose" do
    actual = GildedRose.Appraisal.tokenize("Elixir of the Mongoose")
    expected = ["Elixir", "of", "the", "Mongoose"]

    assert actual == expected
  end

  test "tokenize :: known case - Backstage passes to a TAFKAL80ETC concert" do
    actual = GildedRose.Appraisal.tokenize("Backstage passes to a TAFKAL80ETC concert")
    expected = ["Backstage", "passes", "to", "a", "TAFKAL80ETC", "concert"]

    assert actual == expected
  end

  test "tokenize :: known case - Sulfuras, Hand of Ragnaros" do
    actual = GildedRose.Appraisal.tokenize("Sulfuras, Hand of Ragnaros")
    expected = ["Sulfuras", "Hand", "of", "Ragnaros"]

    assert actual == expected
  end

  test "determine_item_grade :: Conjured item" do
    item = GildedRose.Item.new("Conjured Mana Cake", 3, 6)
    actual = GildedRose.Appraisal.determine_item_grade(item)

    assert actual == [:conjured]
  end

  test "determine_item_grade :: Backstage Pass" do
    item = GildedRose.Item.new("Sulfuras, Hand of Ragnaros", 0, 80)
    actual = GildedRose.Appraisal.determine_item_grade(item)

    assert actual == [:legendary]
  end

  test "determine_item_grade :: Aged" do
    item = GildedRose.Item.new("Aged Brie", 2, 0)
    actual = GildedRose.Appraisal.determine_item_grade(item)

    assert actual == [:aged]
  end
end
