defmodule GildedRoseInventoryAccleratedDecayRegressionTest do
  alias GildedRose.Item.Appraised, as: Appraised
  use ExUnit.Case

  test "regression :: property test found incongruency { :sell_in -11 :quality: 1 :name: Conjured Mana Cake" do
    name = "Conjured Mana Cake"
    item = Appraised.new(GildedRose.Item.new(name, -11, 1))

    v1 =
      Appraised.unwrap_item(
        GildedRoseInventoryAccleratedDecayPropertyTest.v1_apply_accelerated_quality_decay(item)
      )

    v2 = Appraised.unwrap_item(GildedRose.Inventory.apply_accelerated_quality_decay(item))

    expected = {v1.name, v1.quality, v1.sell_in}
    actual = {v2.name, v2.quality, v2.sell_in}

    assert expected == actual
  end

  test "regression :: property test found incongruency { :sell_in -1 :quality: 0 :name: +5 Dexterity Vest" do
    name = "+5 Dexterity Vest"
    item = Appraised.new(GildedRose.Item.new(name, -1, 0))

    v1 =
      Appraised.unwrap_item(
        GildedRoseInventoryAccleratedDecayPropertyTest.v1_apply_accelerated_quality_decay(item)
      )

    v2 = Appraised.unwrap_item(GildedRose.Inventory.apply_accelerated_quality_decay(item))

    expected = {v1.name, v1.quality, v1.sell_in}
    actual = {v2.name, v2.quality, v2.sell_in}

    assert expected == actual
  end
end

defmodule(GildedRoseInventoryAccleratedDecayPropertyTest) do
  alias GildedRose.Item.Appraised, as: Appraised
  use ExUnit.Case
  use ExUnitProperties

  property "apply_accelerage_quality_decay ensure congruency" do
    check all(
            a <- StreamData.integer(-20..20),
            b <- StreamData.integer(0..50),
            c <-
              StreamData.member_of([
                "+5 Dexterity Vest",
                "Aged Brie",
                "Elixir of the Mongoose",
                "Sulfuras, Hand of Ragnaros",
                "Backstage passes to a TAFKAL80ETC concert",
                "Conjured Mana Cake"
              ])
          ) do
      item = Appraised.new(GildedRose.Item.new(c, a, b))
      v1 = Appraised.unwrap_item(v1_apply_accelerated_quality_decay(item))
      v2 = Appraised.unwrap_item(GildedRose.Inventory.apply_accelerated_quality_decay(item))

      expected = {v1.name, v1.quality, v1.sell_in}
      actual = {v2.name, v2.quality, v2.sell_in}

      assert expected == actual
    end
  end

  def v1_apply_accelerated_quality_decay(%Appraised{item: item}) do
    item =
      cond do
        item.sell_in < 0 ->
          cond do
            item.name != "Aged Brie" ->
              cond do
                item.name != "Backstage passes to a TAFKAL80ETC concert" ->
                  cond do
                    item.quality > 0 ->
                      cond do
                        item.name != "Sulfuras, Hand of Ragnaros" ->
                          %{item | quality: item.quality - 1}

                        true ->
                          item
                      end

                    true ->
                      item
                  end

                true ->
                  %{item | quality: item.quality - item.quality}
              end

            true ->
              cond do
                item.quality < 50 ->
                  %{item | quality: item.quality + 1}

                true ->
                  item
              end
          end

        true ->
          item
      end

    Appraised.new(item)
  end
end
