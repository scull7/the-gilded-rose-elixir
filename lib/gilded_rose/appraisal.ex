# This module deals with the classification of items
#
# I'm using this module to implement the "Parse, don't validate" way of
# dealing with system input.

# This is sort of a monkey patch over the item. This may show my rust
# with Elixir as I feel like there must be a better way to integrate this
# using some sort of struct or map combinator.  I'm going with this
# because I want to change the input of the functions without affecting
# the function body.  This way I can handle each function individually.
#
# I'm also using this data wrapper so that we aren't affected by changes
# in the GildedRose.Item internals.
#
# # I'm choosing to use the term `Grade` to in place of `class` because I feel
# that `Class` is a bit too overloaded withing programming.
defmodule GildedRose.Item.Appraised do
  defstruct grade: nil, item: nil

  def new(item) do
    %__MODULE__{grade: GildedRose.Appraisal.determine_item_grade(item), item: item}
  end

  # this allows us to neatly fit our unwrap within the processing pipeline.
  def unwrap_item(appraised) do
    appraised.item
  end

  def apply_quality_growth(appraised, growth) do
    new(%{appraised.item| quality: appraised.item.quality + growth})
  end

  def apply_quality_decay(appraised, decay) do
    new(%{appraised.item| quality: appraised.item.quality - decay})
  end

  def zero_out_quality(appraised) do
    new(%{appraised.item| quality: 0})
  end

  def apply_sell_in_decay(appraised, decay) do
    new(%{appraised.item| sell_in: appraised.item.sell_in - decay})
  end
end

defmodule GildedRose.Appraisal do
  # Given an item name we want to turn it into a set of tokens.
  # The tokens in our case are words.  Currently we consider word characters
  # to be everything except a space (" ") and comma (","). Having this as a 
  # separate function means that we can test and improve our tokenization
  # over time as we discover new items don't fit within our current pattern.
  # For example, one thing we may wish to do is normalize our string to use only
  # upper or lower case characters.
  def tokenize(name) do
    name
    |> String.split([",", " "])
    |> Enum.filter(fn x -> String.length(x) > 0 end)
  end

  # I have added the ability to classify items beyond `Conjured` items.
  #
  # Based upon feedback I'm only going to consider the known cases.
  def determine_item_grade(item) do
    case tokenize(item.name) do
      ["Conjured" | _] -> [:conjured]
      ["Aged" | _] -> [:aged]
      ["Backstage", "passes" | _] -> [:backstage]
      ["Sulfuras" | _] -> [:legendary]
      _ -> [:normal]
    end
  end
end
