# This module will be a collection of inventory management fuctions. I generally
# don't like function grab bags, but it's going to be better than the current implementation
# because we will have the option of testing each function independently.
#
# Also, this organization style will allow for the logic to stil remain in
# a single module as we refactor things.
#
# Since naming things is hard, I've chosen the first name that came to mind, 
# but normally I would ask my teammates what they would call it to narrow
# down a better name if one exists.

defmodule GildedRose.Inventory do
  alias GildedRose.Item.Appraised, as: Appraised
  # I'm hoping that this step-by-step pipeline will impart more meaning to 
  # what the update_item_quality is actually doing.
  def update_item_quality(item) do
    item
    |> Appraised.new()
    |> apply_standard_quality_decay()
    |> apply_sell_in_decay()
    |> apply_accelerated_quality_decay()
    |> apply_conjured_quality_decay()
    |> Appraised.unwrap_item()
  end

  # This is the simplest of the sectioned algorithm steps, so I'm going to 
  # pull out the logic first.
  def apply_sell_in_decay(appraised) do
    case appraised.grade do
      [:legendary] ->
        appraised

      _otherwise ->
        Appraised.apply_sell_in_decay(appraised, 1)
    end
  end

  def apply_standard_quality_decay(appraised) do
    case {appraised.grade, appraised.item.quality, appraised.item.sell_in} do
      {[:normal], quality, _} when quality > 0 ->
        Appraised.apply_quality_decay(appraised, 1)

      {[:conjured], quality, _} when quality > 0 ->
        Appraised.apply_quality_decay(appraised, 1)

      {[:aged], quality, _} when quality < 50 ->
        Appraised.apply_quality_growth(appraised, 1)

      {[:backstage], quality, sell_in} when quality < 50 ->
        prescribed_growth =
          cond do
            sell_in < 6 -> 3
            sell_in < 11 -> 2
            true -> 1
          end

        max_growth_potential = 50 - quality
        actual_growth = min(prescribed_growth, max_growth_potential)

        Appraised.apply_quality_growth(appraised, actual_growth)

      _otherwise ->
        appraised
    end
  end

  # This function applies the additional decay when the _sell_in_ value
  # is less than zero. I'm calling it the accelerated decay.
  def apply_accelerated_quality_decay(appraised) do
    case {appraised.grade, appraised.item.quality, appraised.item.sell_in} do
      {[:aged], quality, sell_in} when quality < 50 and sell_in < 0 ->
        Appraised.apply_quality_growth(appraised, 1)

      {[:backstage], _, sell_in} when sell_in < 0 ->
        Appraised.zero_out_quality(appraised)

      {[:normal], quality, sell_in} when quality > 0 and sell_in < 0 ->
        Appraised.apply_quality_decay(appraised, 1)

      {[:conjured], quality, sell_in} when quality > 0 and sell_in < 0 ->
        Appraised.apply_quality_decay(appraised, 1)

      _otherwise ->
        appraised
    end
  end

  # "Conjured" decay is an additive decay on top of a normal item's decay.
  # Therefore, it's possible to add it as an additional decay processing step.
  # We are keeping this for now because our tests are anemic at this point.
  #
  # ## Caveats
  # - This works with the given test case of _"Conjured Mana Cake"_, however,
  #   it explicitly excludes multiple modifiers, e.g. "Conjured Aged Brie"
  #   as it's not specified how such an item should behave.
  # - This does only considers items whose name begins with "Conjured", I have
  #   made the assumption that categorization is done this way based upon the
  #   existing code.
  #
  # NOTE: I've chosen to keep with the spirit of the above code for now.
  # With the notable exception that we're using a `case` clause for clarity
  def apply_conjured_quality_decay(appraised) do
    case {appraised.grade, appraised.item.quality, appraised.item.sell_in} do
      {[:conjured], quality, sell_in} when quality > 1 and sell_in < 0 ->
        Appraised.apply_quality_decay(appraised, 2)

      {[:conjured], quality, _} when quality > 0 ->
        Appraised.apply_quality_decay(appraised, 1)

      _ ->
        appraised
    end
  end
end
